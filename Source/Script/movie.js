
$(document).ready( function(){
	DetailMovie();
});

function DetailMovie(){
	let url=$(location).attr('href');
    let mArr=url.split('&');
    $.ajax({
             type:"GET",
             url: `https://api.themoviedb.org/3/movie/${mArr[1]}?api_key=aa065c9a7b16af4c48f67b6c9df66fe8`,
			success:function(dataMovie)
			 {
			    console.log(dataMovie);
			    
			    $("#main").empty();
		        $("#main").append(`  
		        				<div class="container">                       
		   
		                            <div class="row ">
		                           				                                
	                                    <div class="col-md-6" style="padding-left:250px;padding-top:10px;">
	                                        <h3>${dataMovie.title}</h3>
	                                        <p > Country: ${dataMovie.production_countries[0].name}</p>
	                                        <p> Release: ${dataMovie.release_date}</p>
	                                        <p> Language ${dataMovie.spoken_languages[0].name}</p>
	                                        <p> Vote: ${dataMovie.vote_average}</p>
	                                        <p> Time: ${dataMovie.runtime}</p>
	                                        <p id="director"><p>
	                                        <p id="genres"> <p>
	                                    </div>
	                                    <div class="col-md-6 img">
		                           			<img class="img-responsive" src="https://image.tmdb.org/t/p/w500/${dataMovie.poster_path}" alt="image actor" style="width:350px;height:500px;">
		                           		</div>
		                            </div>
		                        	<p style="padding-left:250px;">Review:</p>
		                        	<hr>
		                        	<p ">${dataMovie.overview}</p>
		                        	<h4 class="">Các diễn Viên tham gia</h4>
		                        			<hr />
		                        	<div class="container">
		                        		<div class="row actor">
		                        			
		                        		</div>
		                        		
		                        	</div>
		                        	
								</div>
                       			 `);
		        for (let i=0;i<dataMovie.genres.length;i++)
		        	{
    				$("#genres").append(dataMovie.genres[i].name+", " );
					}
               }	

       });
       $.ajax({
       		type:"GET",
            url:`https://api.themoviedb.org/3/movie/${mArr[1]}/credits?api_key=aa065c9a7b16af4c48f67b6c9df66fe8`,
            success:function(listcast)
            {
            	console.log(listcast)
			    for(let i=0;i<listcast.crew.length;i++)
			    {
			    	if(listcast.crew[i].job=="Director"){
			    		$("#director").append(`Director: ${listcast.crew[i].name}`);
			    	}
			    	
			    }
			    for(let i=0;i<50;i++)
			    {
			    	$(".actor").append(`
								  <div class="col-md-3 card">
								  	<a href="./Actor.html?&${listcast.cast[i].id}">
									<img style="width:245px; height:350px;" src="https://image.tmdb.org/t/p/w500/${listcast.cast[i].profile_path}" alt="">
									<h3 style="font-size:24px;">${listcast.cast[i].character}</h3>
									<p>${listcast.cast[i].name}</p> 
									</a>
								   </div>
			    	`);
			    }			    
            }  
              
        })	
       $.ajax({
       		type:"GET",
            url:`https://api.themoviedb.org/3/movie/${mArr[1]}/reviews?api_key=aa065c9a7b16af4c48f67b6c9df66fe8`,
            success:function(listReview)
            {
            	console.log(listReview)
			    for (let i=0;i<listReview.results.length;i++)
              	{
                $("#review").append(`
                	<br>
                	<div class="col-md-12" onclick="url('${listReview.results[i].url}')" data-url="${listReview.results[i].url}" style="cursor:default;border-bottom:solid">
  					<div >
   						<h5>${listReview.results[i].author}</h5>
    					<p>${listReview.results[i].content}</p>
 				    </div>
					</div>	
				`);  
            	}  
            } 
        })	
}