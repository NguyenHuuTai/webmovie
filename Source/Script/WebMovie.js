
 var page;
 var pagesearch; 
 



 $(document).ready( function(){  

 	clickpage();
 	});
  
 function loadsearch(){
	var input=$("#search1").val();
	if(input=="")
	{
		LoadPageMovie();
	}
	else
	{
    $.ajax({
              url:`https://api.themoviedb.org/3/search/multi/?api_key=aa065c9a7b16af4c48f67b6c9df66fe8`,
              type:"GET",
              data:{query:input,page:pagesearch},
              success:function(result)
			 {
			 	    $('body').removeClass("loading");
					$('#preload').delay(1000).fadeOut('fast');

				  
			    console.log(result);
			     $("#main").empty();
            	 var listresult=result.results;
            	 //for
		        for (let i=0;i<listresult.length;i++)
		         {

			 		if(listresult[i].media_type == "movie" && ((listresult[i].title.toUpperCase()).indexOf($("#search1").val().toUpperCase())!= -1))
			 		{
			 			$("#main").append(`                         
                        <div class="col-sm-6 card " style="width: 18rem" >
                            <div class="row ">
                                <img class="col-sm-6 card-img-top " src="https://image.tmdb.org/t/p/w500/${listresult[i].poster_path}" alt="Card image cap">
                                    <div class="col-sm-6">
                                        <h5 class="card-title">${listresult[i].title}</h5>
                                        <p>Rating: ${listresult[i].vote_average}</p>
                                        <p>Vote count: ${listresult[i].vote_count}</p>
                                        <p class="card-text">${listresult[i].overview}</p>        
                                        <p>Release: ${listresult[i].release_date}</p>
                                        <a  class="btn btn-danger" href="./Movie.html?&${listresult[i].id}" >Detail</a>
                                    </div>
                             </div>
                        </div>`);
			 		}
			 		if(listresult[i].media_type == "person" && ((listresult[i].name.toUpperCase()).indexOf($("#search1").val().toUpperCase())!= -1))
			 		{
			 			 $("#main").append(`
			 			 	<div class="col-sm-6 card " style="width: 18rem" >
                                <div class="row ">
                                <img class="col-sm-6 card-img-top " src="https://image.tmdb.org/t/p/w500/${listresult[i].profile_path}" alt="Card image cap">
                                <div class="col-sm-6">
                                  <h5 class="card-title">${listresult[i].name}</h5>
                                  <p class="card-text">Deparment: ${listresult[i].known_for_department}</p>
                                  <a href="./Actor.html?&${listresult[i].id}"><button  class="btn btn-danger" >Detail</button><a>
                                </div>
                              </div>
                      </div>`); 
			 		}	 	

			 	 }//end for
			 	  $('body').addClass("loading");
					$('#preload').show();

                }//end sussec
                
        });// end Ajax
	}	
         
}

function LoadPageMovie()
{
	 $.ajax({
             type:"GET",
             url: `https://api.themoviedb.org/3/movie/popular?api_key=aa065c9a7b16af4c48f67b6c9df66fe8&page=${page}`,
			success:function(data)
			 {
			 	
			    console.log(data);
			    var listdatamovie=data.results; 
			    $("#main").empty();
		        for (let i=0;i<listdatamovie.length;i++)
		        {
			 		 	 	
			 		$("#main").append(`                         
                        <div class="col-sm-6 card " style="width: 18rem;">
                            <div class="row ">
                                <img class="col-sm-6 card-img-top " src="https://image.tmdb.org/t/p/w500/${listdatamovie[i].poster_path}" alt="Card image cap">
                                    <div class="col-sm-6">
                                        <h5 class="card-title">${listdatamovie[i].title}</h5>
                                        <p>Rating: ${listdatamovie[i].vote_average}</p>
                                        <p>Vote count: ${listdatamovie[i].vote_count}</p>
                                        <p class="card-text">${listdatamovie[i].overview}</p>
                                        <p>Release: ${listdatamovie[i].release_date}</p>
                                        <a  class="btn btn-danger" href="./Movie.html?&${listdatamovie[i].id}" >Detail</a>
                                    </div>
                             </div>
                        </div>`);
			 		
			 	}//end for
			 $('body').removeClass("loading");
			$('#preload').delay(1000).fadeOut('fast');
            }	
            	 		 	
       });


}


function clickpage()
{
	$(".page").click(function(){

    let input=$("#search1").val();
    if (input=="")
    {
       page=$(this).text();
       LoadPageMovie();
    }
        
    else {
       pagesearch=$(this).text();
       loadsearch();
    }
  		});
	$("#btnSearch1").click(function(e){ 
	     e.preventDefault(); 
	     loadsearch();
	});

	//load page
	page=1;      
	pagesearch=1;
	LoadPageMovie(page);
		//Next 
	$("#Next").click(function(){
    let input=$("#search1").val();
      if (input=="")
      {
      	page=parseInt(page)+1;			           	
       	LoadPageMovie();			                            
        
      }
        
      else {       
        pagesearch=parseInt(pagesearch)+1;
        loadsearch();
      }
 	});
		//end Next
		//Previous
	 $("#Previous").click(function(){
    let input=$("#search1").val();
      if (input=="")
      {
        if (page>1)
        {
          page=parseInt(page)-1;                 
          LoadPageMovie();
        }
      }
        
      else {   
        if (pagesearch>1)
        {    
	        pagesearch=parseInt(pagesearch)-1;
	        loadsearch();
        }
      }
	  });
		 //end Previous
		 	//Paginator

		      //end Paginator

}
